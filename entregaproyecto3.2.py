#!/usr/bin/env python     
# -*- coding: utf-8 -*-

import random #contiene una serie de funciones relacionadas con los valores aleatorios.
import os #permite acceder a funcionalidades dependientes del Sistema y aquellas que nos 
# refieren información sobre el entorno del mismo y nos permiten manipular la estructura de directorios
import time #proporciona un conjunto de funciones para trabajar con tiempo, "en segundos"

def juego_de_la_vida():
    
    #Definimos las constantes, numero de filas y numero de columnas
    #donde gacias a ello, se puede montar una tabla de cualquier tamaño,
    #manteniendo totalmente operativo el programa.
    
    filas = 20
    columnas = 20
    
    matriz = [
                 [0,0,0],
                 [0,0,0],
                 [0,0,0]
             ]
    #Declaración de variables
    matriz_inicial = []
    matriz_nueva = []
 
    matriz_inicial_completa(matriz_inicial,filas,columnas)
    contar_vivos(matriz_inicial)
    
    matriz_nueva_completa(matriz_nueva,filas,columnas)
    contar_vivos(matriz_nueva)
 
    lista_comprobable(matriz_inicial, matriz_nueva, matriz, filas, columnas)
    
																	#
def lista_comprobable(matriz_inicial ,matriz_nueva , matriz , fila_final, columna_final):

    while True:
        #Recorremos la matriz teniendo en cuenta que no se salga del parametro bidimencional
        for filas in range(fila_final -2):
            for columnas in range(columna_final -2):
               
               #Se recorren las posiciones de la matriz 3x3
                posicion_celula1 = matriz_inicial[filas][columnas]
                posicion_celula2 = matriz_inicial[filas][columnas + 1]
                posicion_celula3 = matriz_inicial[filas][columnas + 2]
                
                posicion_celula4 = matriz_inicial[filas + 1][columnas]
                posicion_celula5 = matriz_inicial[filas + 1][columnas + 1]
                posicion_celula6 = matriz_inicial[filas + 1][columnas + 2]
                
                posicion_celula7 = matriz_inicial[filas + 2][columnas]
                posicion_celula8 = matriz_inicial[filas + 2][columnas + 1]
                posicion_celula9 = matriz_inicial[filas + 2][columnas + 2]
                
                #Las posiciones se interpretan: Primer numero--> Fila, Segundo numero-->Columna
                matriz[0][0] = posicion_celula1
                matriz[0][1] = posicion_celula2
                matriz[0][2] = posicion_celula3
     
                matriz[1][0] = posicion_celula4
                matriz[1][1] = posicion_celula5
                matriz[1][2] = posicion_celula6
                 
                matriz[2][0] = posicion_celula7
                matriz[2][1] = posicion_celula8
                matriz[2][2] = posicion_celula9
                 
                contador_posicion_celular = 0
                  
                for matriz_filas in range(3):
                    for matriz_columnas in range(3):
						
                #Si ya estaba muerta se mantiene, y contamos a las vivas
                #una posicion añadida, asi aseguramos que se impriman el
                #numero de celulas vivas seleccionado. Por ello sumo uno
                #al contador*/          
                        if not (matriz_filas == 1 and matriz_columnas == 1):
                            if matriz[matriz_filas][matriz_columnas] == 1:
                                contador_posicion_celular = contador_posicion_celular + 1
                                
                # Control de las celulas vecinas vivas
                 
                if contador_posicion_celular < 2 and matriz[1][1] == 1:
                    matriz_nueva[filas + 1][columnas + 1] = 0
                 # Si se cumple la condicion, mueren.     
                elif contador_posicion_celular > 3 and matriz[1][1] == 1:
                    matriz_nueva[filas + 1][columnas + 1] = 0
                # Si se cumple la condicion, mueren.  
                elif contador_posicion_celular == 3 and matriz[1][1] == 0:
                    matriz_nueva[filas + 1][columnas + 1] = 1
                #Las celulas muertas con 3 celulas vivas pegadas, resucitan.
                elif contador_posicion_celular == 3 and matriz[1][1] == 1:
                    matriz_nueva[filas + 1][columnas + 1] = 1
                # //Las celulas muertas con 3 celulas vivas pegadas, resucitan.
                elif contador_posicion_celular == 2:
                    matriz_nueva[filas +1][columnas + 1] = matriz_inicial[filas +1][columnas +1]
                #La celulas vivas con 2 o 3 celulas vivas pegadas, se mantiene vivas.
                 
          
        # Devolvemos los nuevos datos a la matriz original
           
        tabla_auxiliar = matriz_inicial
        matriz_inicial = matriz_nueva
        matriz_nueva = tabla_auxiliar
            
        #funcion para limpiar la pantalla     
        os.system('clear')
        #variables que iran cambiando a medida que se modifica la matriz
        relleno_de_tabla(matriz_nueva, fila_final , columna_final)
        
        limpiar_tabla(matriz_nueva, fila_final , columna_final)
        

        time.sleep(2.4)
       
            


def relleno_de_tabla(matriz_inicial , fila_final , columna_final):
     #copiamos la matriz en una copia auxiliar
	for filas in range(fila_final):
		print("| ", end = '')
		for columnas in range(columna_final):
		   #Si es igual a 1 la celula esta viva, por lo que se imprime una "X"
			if matriz_inicial[filas][columnas] == 1:
				print (" X |" , end='')
			# Si no, esta muerta, por lo que se imprime un espacio en blanco          
			else:
				print ("   |" , end='')
			#Se utiliza un "end" para que no provoque un cambio de línea y deje el 
			#cursor para que el siguiente print() continúe justo donde acabó el anterior        
			if columnas == columna_final -1: print(' ')
	contar_vivos(matriz_inicial)
      

def matriz_inicial_completa(matriz_inicial , fila_final , columna_final):
   #Copiamos la matriz inicial, osea lade origen en destino
    for filas in range(fila_final):
        for columnas in range(columna_final):

			#guardamos la columnaen un arreglo, para que se concatene
            arreglo_de_lista = []
            for valor_zero in range(columna_final):
                arreglo_de_lista.append(0)
          
            matriz_inicial.append(arreglo_de_lista)

    #modo aleatorio donde se utiliza en valor 1 y 0 para rellenar la matriz
    for filas in range(fila_final):
        for columnas in range(columna_final):
            valor = random.randint(0,1)

            if valor == 1:
                matriz_inicial[filas][columnas] = 1
    return matriz_inicial

  # Copiamos la matriz origen en destino
def matriz_nueva_completa(matriz_nueva , fila_final , columna_final):
    
    for filas in range(fila_final):
        for columnas in range(columna_final):
           
            #guardamos la columna un arreglo, para que se concatene
            arreglo_de_lista = []
            for valor_zero in range(columna_final):
                arreglo_de_lista.append(0)
            
            matriz_nueva.append(arreglo_de_lista)
    
    for filas in range(fila_final):
        for columnas in range(columna_final):
            matriz_nueva[filas][columnas] = 0
    return matriz_nueva


# tabla auxiliar que esta llena de espacios
def limpiar_tabla(tabla , fila_final , columna_final):
    for filas in range(fila_final):
        for columnas in range(columna_final):
            tabla[filas][columnas] = 0
            
#se cuentan las matrices cada vez que cambian           
def contar_vivos(matriz):
	cont=0
	for i in range(20):
		for j in range(20):
			if (matriz[i][j]==1):
				cont = cont +1
	print("Las celulas vivas son: ",cont)
	print("Las celulas muertas son: ",(20*20)-cont)
# se llama al la funcion principal definida 
juego_de_la_vida()
